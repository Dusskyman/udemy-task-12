import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:udemy_course_task_12/providers/data_provider.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider(
      create: (_) => DataProvider(),
      child: MaterialApp(
        home: MyHomePage(),
      ),
    );
  }
}

class MyHomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    var data = Provider.of<DataProvider>(context, listen: false);
    return Scaffold(
        body: Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          SizedBox(
            width: double.infinity,
            height: MediaQuery.of(context).size.height * 0.9,
            child: Consumer<DataProvider>(
              builder: (context, value, child) {
                return ListView.builder(
                  scrollDirection: Axis.horizontal,
                  itemCount: value.data.length,
                  itemBuilder: (context, index) {
                    return Container(
                      padding: EdgeInsets.all(5),
                      width: 100,
                      height: double.infinity,
                      color: index.isOdd ? Colors.green : Colors.brown,
                      child: Center(child: Text(value.data[index])),
                    );
                  },
                );
              },
            ),
          ),
          Container(
            height: MediaQuery.of(context).size.height * 0.1,
            color: Colors.grey,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                ClipRRect(
                  borderRadius: BorderRadius.circular(50),
                  child: ColoredBox(
                    color: Colors.blue,
                    child: IconButton(
                        icon: Icon(Icons.add),
                        onPressed: () {
                          data.addElement();
                        }),
                  ),
                ),
                Consumer<DataProvider>(
                  builder: (context, value, child) =>
                      Text('Current length: ${value.data.length}'),
                ),
                ClipRRect(
                  borderRadius: BorderRadius.circular(50),
                  child: ColoredBox(
                    color: Colors.blue,
                    child: IconButton(
                        icon: Icon(Icons.remove),
                        onPressed: () {
                          data.removeElement();
                        }),
                  ),
                )
              ],
            ),
          ),
        ],
      ),
    ));
  }
}
