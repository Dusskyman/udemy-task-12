import 'package:flutter/material.dart';

class DataProvider with ChangeNotifier {
  List<String> _data = List.generate(20, (index) => 'My Number №$index');

  List get data {
    return _data;
  }

  void addElement() {
    _data.add('My Number №${_data.length + 1}');
    notifyListeners();
  }

  void removeElement() {
    _data.remove(_data.last);
    notifyListeners();
  }
}
